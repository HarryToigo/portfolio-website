/*
 * substrate.js
 *
 * The substrate module for the Reactified version of the Substrate.js web app,
 * in the Portfolio section of my personal website.
 *
 * Copyright © 2021 Harry H. Toigo II
 *
 * Based largely on the 'substrate' program in the Processing programming
 * language by Jared S Tarbell, written in 2003, and published online at
 * http://www.complexification.net.
 *
 * This file is part of Substrate.js.
 *
 * Substrate.js is free software: you can redistribute it and / or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Substrate.js is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Cabinet Calc.If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * The substrate() function.
 *
 * This is the main exported function of this module. It sets everything up and
 * returns an App object which is then used for controlling everything.
 */

export default function substrate(canvas) {
  const app = new App(canvas);
  return app;
}

/*
 * The App type.
 */

function App(canvas) {
  // An App manages a substrate and controls its drawing.

  // Methods:
  // init():              Initialize the app and substrate.
  // eraseCanvas():       Erase the drawing canvas.
  // loadColors(file):    Load color palette from the given image file.
  // stop():              Stop drawing.
  // start():             Start drawing.
  // update():            Update the substrate, growing all cracks.

  // Properties:
  // canvas:              The HTML canvas element on which this app draws.
  // isDrawing:           True if drawing is proceeding; false otherwise.
  // intervalID:          The ID of the interval timer that drives updates.
  // substrate:           The Substrate object this App controls.

  // Methods

  this.init = () => {
    // Wait for colorPalette.colors to be defined before calling
    // substrate.init() because otherwise an error occurs when the first Crack
    // is created and creates its SandPainter which tries to get a color from
    // the undefined color palette.
    if (!this.substrate.colorPalette.colors) {
      // Try again in 300 ms.
      setTimeout(this.init, 300);
    } else {
      this.substrate.init();
      this.eraseCanvas();
    }
  };

  this.eraseCanvas = () => {
    // Erase the entire drawing canvas to white.
    const ctx = this.canvas.getContext('2d');
    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
  };

  this.loadColors = (imageFile) => {
    // Load colors from the image file passed to the ColorPalette constructor.
    this.substrate.colorPalette = new ColorPalette(imageFile);
    this.init();
  };

  this.stop = () => {
    if (this.intervalID) {
      clearInterval(this.intervalID);
    }
    this.isDrawing = false;
  };

  this.start = () => {
    this.intervalID = setInterval(this.update, 12);
    this.isDrawing = true;
  };

  this.update = () => {
    // Update the app's state (done at each tick of the timer).
    this.substrate.growAllCracks();
  };

  // Properties

  this.canvas = canvas;
  this.isDrawing = false;
  this.intervalID = null;
  this.substrate = new Substrate(this.canvas);

  this.init();
}

/*
 * The Substrate type.
 */

function Substrate(canvas) {
  // The substrate is a 2D grid of vertices, somewhat like a crystal lattice.
  // Cracks can form at 'defects' in the grid, denoted by a number between zero
  // and 360, which represents a number of degrees and the direction of weakness
  // at that point. Normal points are represented by the sentinel value -1. This
  // grid is represented by a two-dimensional array.

  // The cracks in the substrate are held in a linear array, which begins empty.
  // maxCracks puts a limit on the number of cracks we can have.

  // Methods:
  // init():
  // startNewCrack():       Start a new crack growing in this substrate.
  // growAllCracks():       Grow every crack in this substrate by one point.

  // Properties:
  // canvas:                The canvas on which this substrate is drawn.
  // width:                 The width of this substrate and its canvas.
  // height:                The height of this substrate and its canvas.
  // numInitialDefects:     The number of defects a substrate starts with.
  // numStartingCracks:     The number of growing cracks at the beginning.
  // maxCracks:             The maximum number of cracks allowed.
  // cracks:                A linear array of Crack objects in this substrate,
  //                        begins empty.
  // substrate:             A 2D array of integers denoting the lattice points
  //                        of this substrate.
  // colorPalette:          The global color palette used by all sand painters.

  // Methods

  this.init = () => {
    // Clear the substrate grid of defects/cracks.
    this.substrate.forEach((row) => row.fill(-1));

    // Make some defects in the substrate at random lattice points. These will
    // be the points where cracks can begin to grow.
    for (let k = 0; k < this.numInitialDefects; k++) {
      // Pick a random row and column.
      const row = getRandomInt(this.height);
      const col = getRandomInt(this.width);
      this.substrate[row][col] = getRandomInt(360);
    }
    // Empty the cracks list.
    // Note: this should make all the Crack objects that were previously in the
    // array unreachable, so the GC will reclaim their memory.
    this.cracks = [];

    // Start with a few cracks growing.
    for (let k = 0; k < this.numStartingCracks; k++) {
      this.startNewCrack();
    }
  };

  this.startNewCrack = () => {
    // If we haven't yet reached the maximum number of cracks, Create a new
    // Crack object and add it to the cracks array. Calling startNewCrack() when
    // we've already reached the maximum cracks is not an error; we just ignore
    // the request to create a new one.
    if (this.cracks.length < this.maxCracks) {
      this.cracks.push(new Crack(this));
    }
  };

  this.growAllCracks = () => {
    // Grow all the cracks in this substrate.
    this.cracks.forEach((crack) => crack.grow());
  };

  // Properties

  this.canvas = canvas;
  this.width = this.canvas.width;
  this.height = this.canvas.height;
  this.numInitialDefects = 16;
  this.numStartingCracks = 3;
  this.maxCracks = 200;
  this.cracks = [];
  this.substrate = new Array(this.height);
  for (let i = 0; i < this.substrate.length; i++) {
    this.substrate[i] = new Array(this.width);
  }
  // This loads colors from the image file passed in to the constructor.
  this.colorPalette = new ColorPalette('/pollockShimmering.gif');
}

/*
 * The Crack type.
 */

function Crack(substrate) {
  // A crack growing from a seed point through the substrate.

  // Methods:
  // newStartingPoint():  Return a starting point & direction for a new crack.
  // grow():              Change leadingX, leadingY to grow this crack in
  //                      direction dir, drawing its new point and painting the
  //                      open space to one side.
  // paintOpenSpace():    Sand paint the open space to one side of the crack's
  //                      growing edge.

  // Properties:
  // substrate:           A ref to the substrate this crack is in.
  // leadingX:            X coordinate of the leading edge of the crack.
  // leadingY:            Y coordinate of the leading edge of the crack.
  // dir:                 The direction of travel in degrees.
  // painter:             A SandPainter object for this crack.

  // Methods

  this.newStartingPoint = () => {
    // Return a triple holding the x- and y-coordinates of the new starting
    // point and the fault direction in degrees.
    let result = [];
    let x, y;
    // Find a random fault in the substrate.
    do {
      x = getRandomInt(this.substrate.width);
      y = getRandomInt(this.substrate.height);
    } while (this.substrate.substrate[y][x] === -1);

    const origDir = this.substrate.substrate[y][x];
    // Change the fault's direction by ~90 degrees, either clockwise or
    // counterclockwise with equal probability.
    let newDir;
    const dirChange = 90 + getRandomIntBetweenIncl(-2.0, 2.0);
    if (Math.random() < 0.5) {
      newDir = origDir - dirChange;
    } else {
      newDir = origDir + dirChange;
    }
    result = [
      x + 0.61 * Math.cos((newDir / 180) * Math.PI),
      y + 0.61 * Math.sin((newDir / 180) * Math.PI),
      newDir,
    ];
    return result;
  };

  this.grow = () => {
    // Advance this crack in the fault direction, moving leadingX and leadingY,
    // and draw its new leading edge and sand paint the clear space to one side.
    // If this crack's leading edge reaches the edge of the substrate or another
    // crack, stop its progress in this location and begin cracking from a new
    // starting point.
    //
    // No return value(undefined).
    this.leadingX += 0.42 * Math.cos((this.dir / 180) * Math.PI);
    this.leadingY += 0.42 * Math.sin((this.dir / 180) * Math.PI);
    const fuzz = 0.33;
    const fuzzedX = this.leadingX + getRandomBetween(-fuzz, fuzz);
    const fuzzedY = this.leadingY + getRandomBetween(-fuzz, fuzz);

    // Sand paint the open space to one side of this crack.
    this.paintOpenSpace();
    // Draw the point at the crack's leading edge (fuzzed a little).
    drawPoint(
      this.substrate.canvas,
      fuzzedX,
      fuzzedY,
      'rgba(64, 64, 64, 0.17)'
    );

    // Check if we have gone out of bounds or hit another crack.
    const xInt = Math.round(fuzzedX);
    const yInt = Math.round(fuzzedY);
    if (
      xInt >= 0 &&
      xInt < this.substrate.width &&
      yInt >= 0 &&
      yInt < this.substrate.height
    ) {
      // Within bounds.
      if (
        this.substrate.substrate[yInt][xInt] === -1 ||
        Math.abs(this.substrate.substrate[yInt][xInt] - this.dir) < 5
      ) {
        // (xInt, yInt) is without fault or its fault direction is within 5
        // degrees of this crack's direction; continue this crack's growth.
        this.substrate.substrate[yInt][xInt] = Math.round(this.dir);
      } else {
        // Ran into another crack or fault with a different direction.
        [this.leadingX, this.leadingY, this.dir] = this.newStartingPoint();
        this.substrate.startNewCrack();
      }
    } else {
      // Out of bounds.
      [this.leadingX, this.leadingY, this.dir] = this.newStartingPoint();
      this.substrate.startNewCrack();
    }
  };

  this.paintOpenSpace = () => {
    // Using our sand painter, paint a ray perpendicular to one side of this
    // crack, extending out to fill the open space.
    //
    // No return value (undefined).
    let rayX = this.leadingX;
    let rayY = this.leadingY;
    let spaceIsOpen = true;

    // Find the extent of the open space to the side of this crack.
    while (spaceIsOpen) {
      // Move out along a ray perpendicular to this crack's direction of travel.
      rayX += 0.81 * Math.cos(((this.dir - 90) / 180) * Math.PI);
      rayY += 0.81 * Math.sin(((this.dir - 90) / 180) * Math.PI);
      const rayXInt = Math.round(rayX);
      const rayYInt = Math.round(rayY);
      if (
        !(
          rayXInt >= 0 &&
          rayXInt < this.substrate.width &&
          rayYInt >= 0 &&
          rayYInt < this.substrate.height
        ) ||
        this.substrate.substrate[rayYInt][rayXInt] >= 0
      ) {
        // Went out of the substrate bounds or ran into another crack.
        spaceIsOpen = false;
      }
    }
    // Paint the ray with the sand painter.
    this.painter.render(this.leadingX, this.leadingY, rayX, rayY);
  };

  // Properties

  this.substrate = substrate;
  [this.leadingX, this.leadingY, this.dir] = this.newStartingPoint();
  this.painter = new SandPainter(
    this.substrate.canvas,
    this.substrate.colorPalette.getRandomColor()
  );
}

/*
 * The SandPainter type.
 */

function SandPainter(canvas, color) {
  // A painter that paints with "sand" the space to one side of a growing crack.

  // Methods:
  // render(startX, startY, endX, endY):
  //              Draw a sand-painted line from (startX, startY) to (endX, endY).

  // Properties:
  // canvas:            The canvas on which this sand painter paints.
  // color:             The color this sand painter paints with.
  // extent:            A float between 0.0 and 1.0 denoting the percentage of
  //                    the free space to paint with this painter's color.

  // Methods

  this.render = (startX, startY, endX, endY) => {
    // Draw a line from (startX, startY) toward (endX, endY) that extends extent
    // % of the distance and looks like it was painted with sand.

    // Vary the extent a bit between render() calls, but ensure it is between
    // 0.0 and 1.0.
    this.extent += getRandomBetween(-0.05, 0.05);
    this.extent = Math.max(0.0, Math.min(1.0, this.extent));

    // Calculate number of grains by distance.
    // const numGrains = Math.floor(Math.sqrt((endX - startX) ** 2 + (endY - startY) ** 2) / 3);
    const numGrains = 64;
    const step = this.extent / (numGrains - 1);

    // Lay down grains of sand.
    for (let i = 0; i < numGrains; i++) {
      const ptX = startX + (endX - startX) * Math.sin(Math.sin(i * step));
      const ptY = startY + (endY - startY) * Math.sin(Math.sin(i * step));
      const alpha = 0.1 - i / (numGrains * 10.0);

      const fill = `rgba(${this.color.red}, ${this.color.green}, ${this.color.blue}, ${alpha})`;
      drawPoint(this.canvas, ptX, ptY, fill);
    }
  };

  // Properties

  this.canvas = canvas;
  this.color = color;
  this.extent = getRandomBetween(0.01, 0.1);
}

/*
 * The ColorPalette type.
 */

function ColorPalette(imageFile) {
  // A color palette for sand painting. One global color palette object is used
  // by all sand painters. Extract all colors (up to a max of this.maxColors)
  // from the given image file, which is a required parameter.

  // Methods:
  // getColorsFromImage():    Get all colors from an image file & pass the list to callback.
  // getRandomColor():        Return a random color from this palette.

  // Properties:
  // maxColors:               The maximum number of colors we allow in a palette.
  // colors:                  An array of objects denoting colors in our palette.
  //                          This looks like:
  //                            [ {red: 192, green: 64, blue: 32},
  //                              {red: 127, green: 64, blue: 192} ]

  // Methods

  this.getColorsFromImage = (imageFile, callback) => {
    // Extract all colors from the given image file (up to a maximum of
    // this.maxColors) and pass them to the callback function as an array of
    // objects of the form: {red: 12, green: 0, blue: 29}.

    // We need to use a callback here because the loading and decoding of the
    // img element when we set its 'src' property is an asynchronous operation.
    // When this function was synchronous and returned its loaded color palette
    // directly, the line below, this.colors = getColorsFromImage(), executed
    // before the loading operation had finished, and so this.colors was
    // undefined, and resulted in an error when the first SandPainter tried to
    // set its color to colorPalette.getRandomColor().

    const colors = new Set();
    const palette = [];
    const iCanvas = document.createElement('canvas');
    const iCtx = iCanvas.getContext('2d');
    const img = new Image();
    img.crossOrigin = 'anonymous';
    img.src = imageFile;
    img
      .decode()
      .then(() => {
        iCtx.drawImage(img, 0, 0);
        const imgData = iCtx.getImageData(0, 0, iCanvas.width, iCanvas.height);
        const pixels = imgData.data;
        for (let i = 0; i < pixels.length; i += 4) {
          const clr = pixels[i] * 65536 + pixels[i + 1] * 256 + pixels[i + 2];
          // Since colors is a Set and clrs are integers, each color will only
          // be added once:
          colors.add(clr);
          if (colors.size === this.maxColors) {
            break;
          }
        }
        for (let clr of colors) {
          // Convert each integer color like 8069122 to its RGB form,
          // {red: 123, green: 32, blue: 2}.
          const red = Math.floor(clr / 65536);
          const remainder = clr % 65536;
          const green = Math.floor(remainder / 256);
          const blue = clr % 256;
          palette.push({ red: red, green: green, blue: blue });
        }
        callback(palette);
      })
      .catch((err) => {
        console.log('Error loading color palette image file: ', err);
      });
  };

  this.getRandomColor = () => {
    return this.colors[getRandomInt(this.colors.length)];
  };

  // Properties

  this.maxColors = 512;
  this.getColorsFromImage(imageFile, (imgColors) => {
    this.colors = imgColors;
  });
}

/*
 * Drawing functions.
 */

function drawPoint(canvas, x, y, fill) {
  // Draw a circular point at (x, y) on the canvas passed in, using the given
  // fill style.
  const ctx = canvas.getContext('2d');
  ctx.fillStyle = fill;
  ctx.beginPath();
  ctx.arc(x, y, 0.5, 0, 2 * Math.PI);
  ctx.fill();
}

/*
 * Random number functions.
 */

function getRandomInt(max) {
  // Return a random integer between 0 inclusive and max exclusive.
  return Math.floor(Math.random() * max);
}

function getRandomIntBetweenIncl(min, max) {
  // Return a random integer between min inclusive and max inclusive.
  return Math.floor(min + Math.random() * (max - min + 1));
}

function getRandomBetween(min, max) {
  // Return a random float between min inclusive and max exclusive.
  return min + Math.random() * (max - min);
}
