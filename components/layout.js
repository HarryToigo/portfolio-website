import SiteMeta from './siteMeta';
import Navbar from './navbar';
import Footer from './footer';
import styles from '../styles/layout.module.css';

export default function Layout(props) {
  return (
    <div className={styles.layoutContainer}>
      <SiteMeta />
      <Navbar />
      {props.children}
      <Footer />
    </div>
  );
}
