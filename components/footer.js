import Twitter from '../components/twitter';
import GitHub from '../components/gitHub';
import Instagram from '../components/instagram';
import styles from '../styles/footer.module.css';

export default function Footer() {
  return (
    <footer className={styles.footer}>
      <div className={styles.footerContent}>
        <div className={styles.copyrightMsg}>
          <span className={styles.copyrightWord}>Copyright</span> © 2021 Harry
          H. Toigo II
        </div>
        <div className={styles.socialmedia}>
          <a
            href="https://twitter.com/HarryToigo"
            target="_blank"
            rel="noopener noreferrer"
            aria-label="Visit me on Twitter">
            <Twitter />
          </a>
          <a
            href="https://github.com/htoigo"
            target="_blank"
            rel="noopener noreferrer"
            aria-label="Visit my GitHub">
            <GitHub />
          </a>
          <a
            href="https://www.instagram.com/harrytoigo"
            target="_blank"
            rel="noopener noreferrer"
            aria-label="Visit my Instagram">
            <Instagram />
          </a>
        </div>
      </div>
    </footer>
  );
}
