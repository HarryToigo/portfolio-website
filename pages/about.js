import Link from 'next/link';
import styles from '../styles/about.module.css';

export default function About() {
  return (
    <article className={styles.aboutMeScreen}>
      <div className={styles.heading}>
        <h1 className={styles.name}>Harry H. Toigo</h1>
        <h2 className={styles.description}>
          Developer, UX designer, backend engineer, Functional Programmer, and
          DevOps wannabe.
        </h2>
      </div>

      <div className={styles.bio}>
        <p>
          I am a software developer passionate about clear, correct code. I love
          functional programming, Haskell and Lisp.
        </p>
        <p>
          The book{' '}
          <em>The Structure and Interpretation of Computer Programs</em> had a
          formative influence on me during my time at the University of Chicago.
          I still revisit the exercises from time to time, because they are that
          valuable and profound.
        </p>
        <p>
          I am also actively involved in the field of animal welfare, working as
          a vet assistant in a spay/neuter clinic one day a week, doing
          socialization and behavioral modification work at several animal
          shelters in the East Bay, as well as fostering homeless animals.
        </p>
      </div>
    </article>
  );
}
