import Link from 'next/link';
import styles from '../../styles/digitalGarden.module.css';

export default function DigitalGarden() {
  return (
    <article className={styles.digitalGardenScreen}>
      <div className={styles.heading}>
        <h1 className={styles.title}>The Digital Garden</h1>
        <h2 className={styles.description}>
          A collection of code sketches, ideas, scripts, programs, that I am
          cultivating. Some are seedlings, some are young saplings, and some are
          robust redwoods.
        </h2>
      </div>

      <div className={styles.cultivarsList}>
        <Link href="/garden/avatareditor">
          <a className={styles.cultivarCard}>
            <h2>Profile Photo Editor</h2>
            <p>
              A web widget for uploading and editing your profile image or
              avatar.
            </p>
          </a>
        </Link>

        <Link href="/garden/platea">
          <a className={styles.cultivarCard}>
            <h2>Platea dictumst quisque</h2>
            <p>
              Viverra orci sagittis eu volutpat odio facilisis mauris sit amet
              massa vitae tortor condimentum lacinia quis vel eros donec ac odio
              tempor orci. Magna fringilla urna, porttitor rhoncus dolor purus?
            </p>
          </a>
        </Link>
        <Link href="/garden/habitant">
          <a className={styles.cultivarCard}>
            <h2>Habitant morbi tristique</h2>
            <p>
              Augue neque, gravida in fermentum et, sollicitudin ac orci
              phasellus egestas tellus rutrum tellus pellentesque eu tincidunt
              tortor aliquam nulla facilisi cras fermentum, odio eu? Pharetra
              sit amet, aliquam id.
            </p>
          </a>
        </Link>
        <Link href="/garden/mauris">
          <a className={styles.cultivarCard}>
            <h2>Mauris augue</h2>
            <p>
              Nec dui nunc mattis enim ut tellus elementum sagittis vitae et leo
              duis ut diam.
            </p>
          </a>
        </Link>
        <Link href="/garden/orci">
          <a className={styles.cultivarCard}>
            <h2>Orci eu</h2>
            <p>
              Eget arcu dictum varius duis at consectetur lorem donec massa
              sapien, faucibus et molestie ac.
            </p>
          </a>
        </Link>
        <Link href="/garden/sit">
          <a className={styles.cultivarCard}>
            <h2>Sit amet, dictum</h2>
            <p>
              Nunc vel risus commodo viverra maecenas accumsan, lacus vel
              facilisis volutpat, est velit egestas dui, id ornare arcu odio ut!
            </p>
          </a>
        </Link>
        <Link href="/garden/sed">
          <a className={styles.cultivarCard}>
            <h2>Sed enim ut?</h2>
            <p>
              Interdum velit euismod in pellentesque massa placerat duis
              ultricies lacus sed turpis tincidunt id aliquet risus feugiat in
              ante metus.
            </p>
          </a>
        </Link>
      </div>
    </article>
  );
}
