import Link from 'next/link';
import styles from '../styles/home.module.css';

export default function Home() {
  return (
    <article className={styles.homeScreen}>
      <div className={styles.heading}>
        <h1 className={styles.name}>Harry H. Toigo</h1>
        <h2 className={styles.whatIDo}>
          Crafts robust, beautiful experiences through software that is a
          pleasure to maintain and a joy to use.
        </h2>
        <h4 className={styles.roles}>
          Developer, UX designer, backend engineer
        </h4>
      </div>

      <div className={styles.sectionList}>
        <Link href="/portfolio">
          <a className={styles.sectionCard}>
            <h2>Portfolio</h2>
            <p>A selection of some of my work.</p>
          </a>
        </Link>
        <Link href="/garden">
          <a className={styles.sectionCard}>
            <h2>Digital garden</h2>
            <p>
              A continually tended and evolving collection of code, designs,
              notes and other creations.
            </p>
          </a>
        </Link>
        <Link href="/blog">
          <a className={styles.sectionCard}>
            <h2>Blog</h2>
            <p>Shorter posts on coding, design, engineering, etc.</p>
          </a>
        </Link>
        <Link href="/library">
          <a className={styles.sectionCard}>
            <h2>Library</h2>
            <p>A reading collection, with notes.</p>
          </a>
        </Link>
        <Link href="/about">
          <a className={styles.sectionCard}>
            <h2>About me</h2>
            <p>Learn more about me.</p>
          </a>
        </Link>
      </div>
    </article>
  );
}
