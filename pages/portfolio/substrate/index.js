import React, { useEffect } from 'react';
import styles from '../../../styles/substrateScreen.module.css';
import substrate from '../../../lib/substrate';

export default function SubstrateScreen() {
  useEffect(() => {
    const canvasWrapper = document.getElementById('canvasWrapper');
    const canvas = document.getElementById('canvas');
    canvas.width = canvasWrapper.clientWidth;
    canvas.height = canvasWrapper.clientHeight;

    const substrApp = substrate(canvas);

    canvas.addEventListener('click', (event) => {
      if (event.shiftKey) {
        // Start the drawing process over from the beginning.
        substrApp.init();
      } else {
        // Start/stop the drawing process.
        if (substrApp.isDrawing) {
          substrApp.stop();
        } else {
          substrApp.start();
        }
      }
    });

    window.addEventListener('resize', () => {
      canvas.width = canvasWrapper.clientWidth;
      canvas.height = canvasWrapper.clientHeight;
      substrApp.init();
    });
  });

  return (
    <article className={styles.substrateScreen}>
      <div className={styles.heading}>
        <h1 className={styles.title}>SUBSTRATE.JS</h1>
        <h4 className={styles.subtitle}>
          <span>javascript, css, html5</span> <span>aug 2021</span>{' '}
          <span>h.toigo</span>
        </h4>
      </div>

      <div className={styles.description}>
        <p>
          Lines grow like cracks in a crystalline substrate. A simple
          perpendicular growth rule creates intricate city-like structures,
          which are painted by sand.
        </p>

        <p>
          The original <em>Substrate</em> was created by Jared S Tarbell in 2003
          using the{' '}
          <a href="https://processing.org">Processing programming language</a>{' '}
          and published on his website{' '}
          <a href="http://www.complexification.net">Gallery of Computation</a>.
          That site ran <em>Substrate</em> as a Java applet, a technology which
          is no longer supported by modern web browsers. This version has been
          translated to JavaScript, CSS and HTML5 so that, freed of the outdated
          Java applet dependency, it will run on the modern open web.
        </p>
      </div>

      <aside className={styles.instructions}>
        <span>
          <strong>Click</strong> to start/stop drawing.
        </span>
        <span>
          <strong>Shift-Click</strong> to start over.
        </span>
      </aside>

      <div id="canvasWrapper" className={styles.canvasWrapper}>
        <canvas id="canvas"></canvas>
      </div>
    </article>
  );
}
