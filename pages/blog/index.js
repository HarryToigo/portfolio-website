import Link from 'next/link';
import styles from '../../styles/blog.module.css';

export default function Blog() {
  return (
    <article className={styles.blogScreen}>
      <div className={styles.heading}>
        <h1 className={styles.title}>The Blog</h1>
        <h2 className={styles.description}>
          A collection of short-form posts mostly on coding and software
          development and engineering in general.
        </h2>
      </div>

      <div className={styles.postList}>
        <Link href="/blog/sociis">
          <a className={styles.postCard}>
            <h2>Sociis natoque?</h2>
            <p>
              Leo in vitae turpis massa sed elementum tempus egestas sed sed
              risus.
            </p>
          </a>
        </Link>
        <Link href="/blog/condimentum">
          <a className={styles.postCard}>
            <h2>Condimentum vitae?</h2>
            <p>
              Nec sagittis aliquam malesuada bibendum arcu vitae elementum
              curabitur vitae nunc sed.
            </p>
          </a>
        </Link>
        <Link href="/blog/lectus">
          <a className={styles.postCard}>
            <h2>Lectus nulla.</h2>
            <p>
              Cursus metus aliquam eleifend mi in nulla posuere sollicitudin
              aliquam ultrices sagittis?
            </p>
          </a>
        </Link>
        <Link href="/blog/sed">
          <a className={styles.postCard}>
            <h2>Sed blandit!</h2>
            <p>
              Dis parturient montes, nascetur ridiculus mus mauris vitae
              ultricies leo integer malesuada?
            </p>
          </a>
        </Link>
        <Link href="/blog/mauris">
          <a className={styles.postCard}>
            <h2>Mauris nunc.</h2>
            <p>
              Sed nisi lacus, sed viverra tellus in hac habitasse platea
              dictumst vestibulum!
            </p>
          </a>
        </Link>
        <Link href="/blog/rutrum">
          <a className={styles.postCard}>
            <h2>Rutrum quisque?</h2>
            <p>
              Nulla posuere sollicitudin aliquam ultrices sagittis orci, a
              scelerisque purus semper eget.
            </p>
          </a>
        </Link>
        <Link href="/blog/phasellus">
          <a className={styles.postCard}>
            <h2>Phasellus faucibus?</h2>
            <p>
              Ut tristique et, egestas quis ipsum suspendisse ultrices gravida
              dictum fusce ut.
            </p>
          </a>
        </Link>
      </div>
    </article>
  );
}
